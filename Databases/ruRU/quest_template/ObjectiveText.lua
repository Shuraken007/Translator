ruRU_quest_ObjectiveText = {
	["rescue drull"] = "Освободите Друлла",
	["flame of azel charged"] = "Сила пламени Азеля получена",
	["cache of explosives destroyed"] = "Взрывчатка уничтожена",
	["attack plan: valley of trials destroyed"] = "План нападения на Долину Испытаний уничтожен",
	["bael modan flying machine destroyed"] = "Уничтожить ветролет в Бейл Модане",
	["test the dried seeds"] = "Проверьте мертвые семена",
	["shut off main control valve"] = "Перекройте главный клапан управления",
	["destroy the demon seed"] = "Уничтожьте Демоново семя",
	["read the lay of ameth'aran"] = "Прочтите \"Песнь об Амет'Аране\"",
	["destroy the seal at the ancient flame"] = "Уничтожьте печать у древнего пламени",
	["find the hottest area of fire plume ridge"] = "Найдите самую горячую точку Вулкана Огненного Венца",
	["view the tome of mel'thandris"] = "Изучите фолиант Мел'Тандрис",
	["find evidence of the scythe of elune"] = "Найдите доказательство Косы Элуны",
	["free the highborne soul in night run"] = "Освободите душу высокорожденного на Ночной поляне",
	["egg of onyxia destroyed"] = "Уничтожено яиц Ониксии",
	["tears destroyed"] = "Уничтожено кристаллов",
	["toss stink bomb into southshore"] = "Бросьте бомбу-вонючку в Южнобережье",
	["ju-ju heaps destroyed"] = "Уничтожено груд джуджу",
	["gelkak's first mate"] = "Первый помощник Гелхака",
	["rabid thistle bear captured"] = "Поймано бешеных колючешерстных медведей",
	["servants of razelikh freed"] = "Освобождено слуг Разелиха",
	["check first cage"] = "Проверьте первую клетку",
	["burn the highvale records"] = "Сожгите записи Высокодолья",
	["western tower ablaze"] = "Западная башня подожжена",
	["screecher spirits collected"] = "Поймано душ крикунов",
	["information recovered"] = "Информация получена",
	["gor'tesh head planted"] = "Голова Гор'теша установлена",
	["find mankrik's wife"] = "Найдите жену Манкрика",
	["tower one marked"] = "Первая башня отмечена",
	["scare legacki"] = "Напугайте Легакки",
	["extinguish the brazier of pain"] = "Потушите жаровню Боли",
	["darrowshire spirits freed"] = "Освобождено духов Дарроушира",
	["souls freed"] = "Освобождено душ",
	["peons awoken"] = "Проучено батраков",
	["kodos tamed"] = "Приручено Кодо",
	["portals banished"] = "Порталы закрыты",
	["heal and fortify sentinel shaya"] = "Исцелите часовую Шайю и укрепите ее здоровье",
	["heal and fortify guard roberts"] = "Исцелите стражника Робертса и укрепите его здоровье",
	["heal and fortify mountaineer dolf"] = "Исцелите горного пехотинца Дольфа и укрепите его здоровье",
	["heal and fortify grunt kor'ja"] = "Исцелите рубаку Кор'жа и укрепите ее здоровье",
	["heal and fortify deathguard kel"] = "Исцелите стража смерти Кела и укрепите его здоровье",
	["accept redpath's forgiveness"] = "Примите покаяние Редпата",
	["scourge structures destroyed"] = "Уничтожено сооружений Плети",
	["sickly deer cured"] = "Исцелено больных оленей",
	["sickly gazelle cured"] = "Больная газель исцелена",
	["gaea seed planted"] = "Посажено семян Геи",
	["samuel's remains buried"] = "Останки Самуэля погребены",
	["rats captured"] = "Поймано крыс",
	["vylestem vines healed"] = "Исцелено растений гнусь-лозы",
	["graveyard assaulted"] = "Кладбище захвачено",
	["tower captured"] = "Башня захвачена",
	["banner destroyed"] = "Вымпел уничтожен",
	["mine captured"] = "Рудник захвачен",
	["ichman's location discovered"] = "Ромеон найден",
	["guse's location discovered"] = "Смуггл найден",
	["parchment created"] = "Пергамент создан",
	["mine assaulted"] = "Нападение на рудник совершено",
	["frankal questioned"] = "Франкал допрошен",
	["the fate of mistress natalia mar'alith"] = "Судьба госпожи Наталии Мар'алит",
	["who does number two work for?"] = "На кого работает номер два?",
	["find metzen the reindeer and rescue him"] = "Найдите и спасите Метцена – северного оленя",
	["lunar fireworks fired"] = "Ракет запущено",
	["ysida freed"] = "Исида спасена",
	["theldren's team defeated"] = "Команда Телдрена побеждена",
	["atiesh cleansed"] = "Атиеш очищен",
	["flame of dire maul"] = "Пламя Забытого Города",
	["return silithyst"] = "Верните силитист",
	["deliver silithyst"] = "Принесите силитист",
	["capture crown guard tower"] = "Захватите башню Королевской Стражи",
	["rescue tog'thar"] = "Освободите Тог'тара",
	["flame of veraz charged"] = "Сила пламени Вераза получена",
	["attack plan: sen'jin village destroyed"] = "План нападения на деревню Сен'джин уничтожен.",
	["shut off fuel control valve"] = "Перекройте топливный распределительный клапан",
	["visit blue raptor nest"] = "Посетите синее гнездо ящера",
	["read the fall of ameth'aran"] = "Прочтите \"Падение Амет'Арана\"",
	["free the highborne soul in satyrnaar"] = "Освободите душу высокорожденного в Сатирнааре",
	["servants of grol freed"] = "Освобождено слуг Грола",
	["check second cage"] = "Проверьте вторую клетку",
	["open sharpbeak's cage"] = "Откройте клетку Остроклюва",
	["burn the highvale notes"] = "Сожгите заметки Высокодолья",
	["southern tower ablaze"] = "Южная башня подожжена",
	["tower two marked"] = "Вторая башня отмечена",
	["scare sprinkle"] = "Напугайте Поливалку",
	["extinguish the brazier of malice"] = "Погасите жаровню Злобы",
	["archive burned"] = "Архив сожжен",
	["remains of eva sarkhoff burned"] = "Останки Евы Саркофф сожжены",
	["rite of cunning"] = "Ритуал Хитрости",
	["vipore's location discovered"] = "Сквороц найден",
	["jeztor's location discovered"] = "Мааша найдена",
	["lumber mill assaulted"] = "Нападение на лесопилку совершено",
	["rutgar questioned"] = "Рутгар допрошен",
	["lunar fireworks cluster fired"] = "Фейерверков запущено",
	["writ of safe passage signed"] = "Подорожная подписана",
	["flame of blackrock spire"] = "Пламя пика Черной горы",
	["capture eastwall tower"] = "Захватите Восточную башню",
	["hillsbrad proclamation destroyed"] = "Хилсбрадское официальное объявление уничтожено",
	["flame of uzel charged"] = "Сила пламени Узеля получена",
	["attack plan: orgrimmar destroyed"] = "План нападения на Оргриммар уничтожен",
	["shut off regulator valve"] = "Перекройте перепускной клапан",
	["visit yellow raptor nest"] = "Посетите желтое гнездо ящера",
	["servants of allistarj freed"] = "Освобождено слуг Аллистария",
	["check third cage"] = "Проверьте третью клетку",
	["burn the highvale report"] = "Сожгите отчет Высокодолья",
	["eastern tower ablaze"] = "Восточная башня подожжена",
	["tower three marked"] = "Третья башня отмечена",
	["scare quixxil"] = "Напугайте Квикксиля",
	["extinguish the brazier of suffering"] = "Погасите жаровню Страдания",
	["remains of lucien sarkhoff burned"] = "Останки Люсьена Саркофф сожжены",
	["slidore's location discovered"] = "Макарч найден",
	["mulverick's location discovered"] = "Маэстр найден",
	["blacksmith assaulted"] = "Нападение на кузницу совершено",
	["flame of stratholme"] = "Пламя Стратхольма",
	["capture northpass tower"] = "Захватите башню Северного перевала",
	["visit red raptor nest"] = "Посетите красное гнездо ящера",
	["servants of sevine freed"] = "Освобождено слуг Севины",
	["northern tower ablaze"] = "Северная башня подожжена",
	["tower four marked"] = "Четвертая башня отмечена",
	["extinguish the brazier of hatred"] = "Погасите жаровню Ненависти",
	["farm assaulted"] = "Нападение на ферму совершено",
	["stable assaulted"] = "Нападение на стойла совершено",
	["flame of the scholomance"] = "Пламя Некроситета",
	["capture plaguewood tower"] = "Захватите башню Проклятого леса",
}

