## Interface: 11310
## Title: !!!|cffffffffTranslator|r
## Notes: Addon for the translation of the server part of the game
## Notes-ruRU: Аддон для перевода игры
## Version: 2.0
## Author: Artur91425, Shuraken007
## SavedVariables: TR_DB

## X-Date: 03.04.2017
## X-License: BEERWARE License
## X-Website: https://vk.com/wowruru , https://vk.com/id65515748 , https://vk.com/shuraken007,
## X-Category: Interface Enhancements
## X-Commands: /tr, /translator

embeds.xml

init.lua
Locale\enUS.lua
Locale\ruRU.lua

Tooltip.lua
Core.lua

Translate.lua
GUI.lua
Options.lua
Bindings.xml

GlobalStrings.lua
Hook\BlizzardHook.lua
Hook\ApiHook.lua
Hook\HookManager.lua

Databases\ruRU\ruRU.xml
