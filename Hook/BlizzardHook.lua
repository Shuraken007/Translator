﻿local addon_name = ...
local addon = LibStub("AceAddon-3.0"):GetAddon(addon_name)

addon.AutoBlizzardHookData = {
	AuctionFrameAuctions_Update = {
		AuctionsButton = {text_obj_pairs = {p = 'Name'}, {'item_name', ITEM_BONUS = 'item_name'}},
	},
	AuctionFrameBid_Update = {
		BidButton = {text_obj_pairs = {p = 'Name'}, {'item_name', ITEM_BONUS = 'item_name'}}
	},
	AuctionFrameBrowse_Update = {
		BrowseButton = {text_obj_pairs = {p = 'Name'}, {'item_name', ITEM_BONUS = 'item_name'}}
	},
	AuctionsFrameAuctions_ValidateAuction = {
		AuctionsItemButtonName = {{'item_name', ITEM_BONUS = 'item_name'}},
	},
	BankFrame_ShowPanel = {
		BankFrameTitleText = 'creature_Name',
		event = 'BANKFRAME_OPENED'
	},
	ClassTrainerFrame_Update = {
		ClassTrainerNameText = 'creature_Name',
		ClassTrainerGreetingText = 'mangos_string'
	},
	CraftFrame_SetSelection = {
		CraftReagent = {text_obj_pairs = {p = 'Name'}, 'item_name'}
	},
	GossipFrameUpdate = {
		GossipFrameNpcNameText = {{'creature_Name', 'gameobject'}},
		GossipGreetingText = 'npc_text',
		GossipTitleButton  = {text_obj_pairs = {f = NUMGOSSIPBUTTONS}, {COLOR_STR = {nil, 'quest_title'}, 'gossip_menu_option', 'gossip_texts'}, todo = GossipResize}
	},
	GuildRegistrar_OnShow = {
		GuildRegistrarFrameNpcNameText = 'creature_Name'
	},
	ItemTextFrame = {
		OnEvent = {
			ItemTextTitleText = {{'item_name', 'gameobject'}},
			event = 'ITEM_TEXT_READY',
			custom_func = function() addon:ItemTextFrame_OnEvent() end,
		}
	},
	LootFrame_Update = {
		LootButton = {text_obj_pairs = {p = 'Text'}, {'item_name', ITEM_BONUS = 'item_name'}}
	},
	MerchantFrame_UpdateBuybackInfo = {
		MerchantItem = {text_obj_pairs = {p = 'Name'}, {'item_name', ITEM_BONUS = 'item_name'}}
	},
	MerchantFrame_UpdateMerchantInfo = {
		MerchantNameText = 'creature_Name',
		MerchantItem = {text_obj_pairs = {p = 'Name'}, 'item_name'},
		MerchantBuyBackItemName = {{'item_name', ITEM_BONUS = 'item_name'}}
	},
	QuestFrameDetailPanel = {
		OnShow = {
			QuestInfoTitleHeader = 'quest_title',
			QuestInfoObjectivesText = 'quest_objectives',
			QuestInfoDescriptionText = 'quest_details',
			custom_func = function() addon:QuestFrameItems_Update() end,
		},
	},
	QuestFrameGreetingPanel = {
		OnShow = {custom_func = function() addon:QuestFrameGreetingPanel_OnShow() end}
	},
	QuestFrameProgressPanel = {
		OnShow = {
			QuestProgressTitleText = 'quest_title',
			QuestProgressText = 'quest_RequestItemsText'
		},
	},
	QuestFrameRewardPanel = {
		OnShow = {
			QuestInfoTitleHeader = 'quest_title',
			QuestInfoRewardText = 'quest_OfferRewardText',
			custom_func = function() addon:QuestFrameItems_Update() end,
		},
	},
	QuestFrame_SetPortrait = {
		QuestFrameNpcNameText = {{'gameobject', 'creature_Name', 'item_name'}}
	},
    QuestFrameProgressItems_Update = {
        QuestProgressItem = {text_obj_pairs = {field = 'Name'}, 'item_name'},
    },
	QuestLog_UpdateQuestDetails = {
		QuestLogQuestTitle = 'quest_title',
		QuestLogObjectivesText = 'quest_objectives',
		QuestLogQuestDescription = 'quest_details',
		QuestLogObjective = {text_obj_pairs = true, {'quest_EndText', QUEST_LOG_OBECTIVE_COMPLETE = {addon.QUESTLOG_MESSAGE_list}, ["(.+)"] = {addon.QUESTLOG_MESSAGE_list}}},
		custom_func = function() addon:QuestInfo_ShowRewards() end,
	},
	TabardFrame_OnEvent = {
		TaxiMerchant = 'creature_Name',
		event = 'OPEN_TABARD_FRAME'
	},
	TaxiFrame_OnEvent = {
		TaxiMerchant = 'creature_Name',
		event = 'TAXIMAP_OPENED'
	},
	TradeSkillFrame_SetSelection = {
		TradeSkillReagent = {text_obj_pairs = {p = 'Name'}, 'item_name'}
	}
}

-- self:PrintTab(addon.AutoBlizzardHookData)

function addon:AutoCheckObject(obj, list, method, object)
	local gettext_db = {}
	local translate_text = {}
	if type(obj) == 'string' then
		-- print("object: "..obj)
		obj = _G[obj]
	end
	if not obj then return end
	-- addon:PrintTab({obj=obj, list=list, method=method, object = object, text=text}, 2)
	local text = obj:GetText()
	-- self.dump(text)
	if not text then return nil end
	-- Printd("text:"..text)

	local result = self.Translator:Translate(text, list[1], method, object)

	if result then
		obj:SetText(result)
	end
	if list.todo then
		list.todo(obj)
	end
end

function addon:AutoBlizzardHook(method, event, ...)
	local data
	local object
	local custom_func
	if type(method) == 'string' then
		data = self.AutoBlizzardHookData[method]
	elseif type(method) == 'table' then
		object, method = method[1], method[2]
		data = self.AutoBlizzardHookData[object][method]
	end
	local arg = {...}
	if data.event and (not arg[1] or data.event ~= arg[1]) then return end
	for key, list in pairs(data) do
		if key ~= 'event' then
			if type(list) == 'string' then
				list = {list}
			end
			if type(list) == 'table' then
				if list.text_obj_pairs then
					local i = 1 or (type(list.text_obj_pairs) == 'table' and list.text_obj_pairs.i)
					local postfix = (type(list.text_obj_pairs) == 'table' and list.text_obj_pairs.p)
					local finish = (type(list.text_obj_pairs) == 'table' and list.text_obj_pairs.f)
					local field = (type(list.text_obj_pairs) == 'table' and list.text_obj_pairs.field)
					-- Printd("start TextObjPairs for"..key)
					for _, _, obj in self:TextObjPairs(key, i, postfix, finish, field) do
						-- Printd("text_obj_pairs: "..key..i..(postfix or ""))

						self:AutoCheckObject(obj, list, method, object)
					end
				else
					self:AutoCheckObject(key, list, method, object)
				end
			end
		end
	end
	if data.custom_func then
		data.custom_func()
	end
end

function addon:CompactUnitFrame_UpdateName(frame)
	local Name = frame.name
	if self.db.profile.ImaginaryModeOff then
		if Name.restore then
			Name:SetText(Name.restore)
			Name.restore = nil
		end
	else
		local name = Name:GetText()
		local trans = self.Translator:Translate(name, 'creature_Name', 'CompactUnitFrame_UpdateName')
		if trans then
			Name.restore = name
			Name:SetText(trans)
		end
	end
end

function addon:ChatFrame_OnEvent(frame, event, ...)
	local args = {...}
	local arg1 = args[1]
	local arg2 = args[2]
	if self:IsEventInChatGroup(event, 'monster') then
		-- if self:IsEventBubble(event) then self:ScheduleEvent('BubbleFrame_Update' , self.db.profile.bubbles) end
		local text = self.Translator:Translate(arg1, {'script_texts', 'creature_ai_texts', 'dbscript_string'}, 'ChatFrame')
		if text then arg1 = text end
		if arg2 then
			local name = self.Translator:Translate(arg2, 'creature_Name', 'ChatFrame')
			if name then arg2 = name end
		end
	elseif self:IsEventInChatGroup(event, 'battleground') then
		local text = self.Translator:Translate(arg1, 'mangos_string', 'ChatFrame')
		if text then arg1 = text end
	elseif self:IsEventInChatGroup(event, 'player') then
		local result = gsub(arg1, '(|%x-|H.-|h%[)(.-)(%]|h|r)',
			function(prefix, item, postfix)
				local trans = self.Translator:Translate(item, 'item_name', 'ChatFrame')
						   or self:ItemsWithBonus(item, 'ChatFrame')
				if trans then item = trans end
				return prefix..item..postfix
			end)
		arg1 = result
	elseif event == 'CHAT_MSG_TEXT_EMOTE' then
		local text = self.Translator:Translate(arg1, self.EMOTE_TEXT, 'ChatFrame')
		if text then arg1 = text end
	elseif event == 'CHAT_MSG_SYSTEM' then
		local text = self.Translator:Translate(arg1, self.CHAT_MSG_SYSTEM_list, 'ChatFrame')
				  or self.Translator:Translate(arg1, 'mangos_string', 'ChatFrame')
		if text then arg1 = text end
	elseif self:IsEventInChatGroup(event, 'combatlog') then
		local text = self.Translator:Translate(arg1, self.COMBAT_LOG_list, 'ChatFrame')
		if text then arg1 = text end
	end
	args[1] = arg1
	args[2] = arg2
	self.hooks[frame].OnEvent(frame, event, unpack(args))
end

function addon:ContainerFrame_GenerateFrame(frame, size, id)
	local name = self.Translator:Translate(GetBagName(id), 'item_name', 'ContainerFrame_GenerateFrame')
	if name then  _G[frame:GetName()..'Name']:SetText(name) end
end

function addon:GroupLootFrame_OnShow(this, ...)
	local _, item = GetLootRollItemInfo(this.rollID)
	local name = self.Translator:Translate(item, 'item_name', 'GroupLootFrame_OnShow')
			  or self:ItemsWithBonus(item, 'GroupLootFrame_OnShow')
	_G['GroupLootFrame'..this:GetID()..'Name']:SetText(name)
end

function addon:InboxFrame_Update()
	for i, text, str in self:TextObjPairs('MailItem', 1, 'Subject') do
		local item = self.Translator:Translate(text, {'item_name', 'mangos_string'}, 'InboxFrame_Update')
				  or self:ItemsWithBonus(text, 'InboxFrame_Update')
				  or self.Translator:Translate(text, self.AUCTION_SUBJECT_list, 'InboxFrame_Update')
		if item then str:SetText(item) end
	end
end

-- function addon:OnInsert(frame, text) -- for ChatFrameEditBox
-- 	print(1)
-- 	if frame == ChatFrameEditBox then
-- 		text = self.Translator:Translate(text, {ITEM_LINK = {nil, 'item_name'}}, "SetItemRef") or text
-- 	end
-- 	self.hooks[frame].Insert(frame, text)
-- end

function addon:ItemTextFrame_OnEvent()
	local font_obj = ItemTextPageText:GetRegions()
	local text = font_obj:GetText()
	local ITEM_PATTERN = '\n%s\n%s'
	local trans = self.Translator:Translate(text, {'page_text', ITEM_PATTERN = 'page_text'}, 'ItemTextFrame')
	if text then ItemTextPageText:SetText(trans) end
end

function addon:OpenMail_Update()
	if not InboxFrame.openMailID then return end

	local subject = select(4, GetInboxHeaderInfo(InboxFrame.openMailID))
	if subject then
		local item = self.Translator:Translate(subject, 'item_name', 'OpenMail_Update')
				  or self:ItemsWithBonus(subject, 'OpenMail_Update')
				  or self.Translator:Translate(subject, self.AUCTION_SUBJECT_list, 'OpenMail_Update')
		if item then OpenMailSubject:SetText(item) end
	end

	local invoiceType, itemName, _, bid, buyout = GetInboxInvoiceInfo(InboxFrame.openMailID)
	local item = self.Translator:Translate(itemName, 'item_name', 'OpenMail_Update')
			  or self:ItemsWithBonus(itemName, 'OpenMail_Update')
	if item and invoiceType == 'buyer' then
		local buyMode
		if bid == buyout then
			buyMode = '('..BUYOUT..')'
		else
			buyMode = '('..HIGH_BIDDER..')'
		end
		OpenMailInvoiceItemLabel:SetText(ITEM_PURCHASED_COLON..' '..item..'  '..buyMode)
	elseif item then
		OpenMailInvoiceItemLabel:SetText(ITEM_SOLD_COLON..' '..item)
	end
end

function addon:QuestFrameGreetingPanel_OnShow()
	local text = self.Translator:Translate(GetGreetingText(), {'npc_text', 'questgiver_greeting'}, 'QuestFrameGreetingPanel_OnShow')
	if text then GreetingText:SetText(text) end
	for i, text, str in self:TextObjPairs('QuestTitleButton') do
		local title = self.Translator:Translate(text, {COLOR_STR = {nil, 'quest_title'}}, 'QuestFrameGreetingPanel_OnShow')
		if title then
			str:SetText(title)
			str:SetHeight(str:GetTextHeight() + 2)
		end
	end
end

-- I can't hook this Blizzard func, so I call it on custom_func
function addon:QuestFrameItems_Update()
	local prefix = 'QuestInfoRewardsFrameQuestInfoItem'
	for i, text, str in self:TextObjPairs(prefix, 1, nil, nil, 'Name') do
		local trans = self.Translator:Translate(text, 'item_name', 'QuestFrameItems_Update')
		if trans then str:SetText(trans) end
	end
end

-- I can't hook this Blizzard func, so I call it on custom_func
function addon:QuestInfo_ShowRewards()
	local prefix = 'QuestLogItem'
	for i, text, str in self:TextObjPairs(prefix, 1, nil, nil, 'Name') do
		local trans = self.Translator:Translate(text, 'item_name', 'QuestInfo_ShowRewards')
		if trans then str:SetText(trans) end
	end
end

function addon:QuestLog_Update()
	for i, text, str in self:TextObjPairs('QuestLogTitle') do
		local questCheck = _G['QuestLogTitle'..i..'Check']
		local questNormalText = _G['QuestLogTitle'..i..'NormalText']
		if text then
			local title = self.Translator:Translate(text, {QUEST_LOG_TITLE = 'quest_title'}, 'QuestLog_Update')
			if title then
				str:SetText(title)
				QuestLogDummyText:SetText(title)
				questNormalText:SetWidth(275 - 15 - _G['QuestLogTitle'..i..'Tag']:GetWidth())
				local width = min(questNormalText:GetWidth(), QuestLogDummyText:GetWidth())
				questCheck:SetPoint('LEFT', str, 'LEFT', width + 24, 0)
			end
		end
	end
end

function addon:QuestWatch_Update()
	local questWatchMaxWidth = 0
	local list = {QUEST_WATCH_TITLE = {{'quest_EndText',
		QUEST_ITEMS_NEEDED = 'quest_ObjectiveText',
		QUEST_MONSTERS_KILLED = 'creature_Name',
		QUEST_OBJECTS_FOUND = 'item_name'
	}}}
	for i, text, str in self:TextObjPairs('QuestWatchLine') do
		local obj = self.Translator:Translate(text, 'quest_title', 'QuestWatch_Update')
				 or self.Translator:Translate(text, list, 'QuestWatch_Update')
		if obj then str:SetText(obj) end
		local temp_width = str:GetWidth()
		if temp_width > questWatchMaxWidth then
			questWatchMaxWidth = temp_width
		end
	end
	_G['QuestWatchFrame']:SetWidth(questWatchMaxWidth + 10)
	UIParent_ManageFramePositions()
end

function addon:SendMailFrame_Update()
	local subject_text = SendMailSubjectEditBox:GetText()
	local previous_item = SendMailFrame and SendMailFrame.previousItem
	local cached_previous_item = self.tmp.last_subject_test_translate
	if subject_text == previous_item and subject_text ~= cached_previous_item then
		local t_text = self.Translator:Translate(subject_text, {'item_name', STACK_ITEM = 'item_name'}, 'SendMailFrame_Update')
				  or self:ItemsWithBonus(itemName, 'SendMailFrame_Update')
		self.tmp.last_subject_test_translate = t_text or subject_text
		if t_text then
			SendMailSubjectEditBox:SetText(t_text)
			SendMailFrame.previousItem = t_text
		end
	end
end

function addon:StaticPopup_Show(which, text_arg1, text_arg2, data, insertedFrame)
	local list = self.PopupFrame_list
	local translated_arg1 = self.Translator:Translate(text_arg1, list, 'StaticPopup_Show')
	local translated_arg2 = self.Translator:Translate(text_arg2, list, 'StaticPopup_Show')
	if translated_arg1 or translated_arg2 then
		local dialog = StaticPopup_FindVisible(which, data)
		if dialog then
			dialog:Hide()
		end
		StaticPopup_Show(which, translated_arg1 or text_arg1, translated_arg2 or text_arg2, data, insertedFrame)
	end
end


function addon:TradeFrame_UpdatePlayerItem(id)
	if id ~= 7 then
		local name = self.Translator:Translate(GetTradePlayerItemInfo(id), 'item_name', 'TradeFrame_UpdatePlayerItem')
				  or self:ItemsWithBonus(GetTradePlayerItemInfo(id), 'TradeFrame_UpdatePlayerItem')
		if name then _G['TradePlayerItem'..id..'Name']:SetText(name) end
	end
end

function addon:TradeFrame_UpdateTargetItem(id)
	if id ~= 7 then
		local name = self.Translator:Translate(GetTradeTargetItemInfo(id), 'item_name', 'TradeFrame_UpdateTargetItem')
				  or self:ItemsWithBonus(GetTradeTargetItemInfo(id), 'TradeFrame_UpdateTargetItem')
		if name then _G['TradeRecipientItem'..id..'Name']:SetText(name) end
	end
end

function addon:UIErrorsFrame_OnEvent(frame, event, ...)
	local args = {...}
	local message_index = 1 --default
	if frame == UIErrorsFrame then
		local list, db
		if event == 'UI_INFO_MESSAGE' then
			message_index = 2
			list = self.UI_INFO_MESSAGE_list
			db = 'areatrigger_teleport'
		elseif event == 'UI_ERROR_MESSAGE' then
			message_index = 2
			list = self.UI_ERROR_MESSAGE_list
			db = 'mangos_string'
		end

		local message = args[message_index]
		message = self.Translator:Translate(message, list, 'UIErrorsFrame_OnEvent') or self.Translator:Translate(message, db, 'UIErrorsFrame_OnEvent')

		if message then
			args[message_index] = message
			frame:Clear()
			frame:OnEvent(event, unpack(args))
		end
	end
end

function addon:OnEvent(frame, ...)
	if frame == UIErrorsFrame then
		self:UIErrorsFrame_OnEvent(frame, ...)
	elseif string.match(frame:GetName(), 'ChatFrame') then
		self:ChatFrame_OnEvent(frame, ...)
	end
end

function addon:UnitFrame_Update(this, ...)
	local name = self.Translator:Translate(GetUnitName(this.unit), 'creature_Name', 'UnitFrame_Update')
	if name then this.name:SetText(name) end
end
