﻿local addon_name = ...
local addon = LibStub("AceAddon-3.0"):GetAddon(addon_name)

function addon:get_available_chat_frame_objects()
	local chat_frames = {}
	for i = 1, NUM_CHAT_WINDOWS do
		local chat_frame_name = 'ChatFrame'..i
		if _G[chat_frame_name] then
			tinsert(chat_frames, _G[chat_frame_name])
		end
	end
	return chat_frames
end

function addon:hook_chat_frames()
	local chat_frames = self:get_available_chat_frame_objects()
	local handler = function(...)
		self['OnEvent'](self, ...)
	end
	for _, chat_frame in ipairs(chat_frames) do
		if not self:IsHooked(chat_frame) then
			self:RawHookScript(chat_frame, "OnEvent", handler)
		end
	end
end

function addon:unhook_chat_frames()
	local chat_frames = self:get_available_chat_frame_objects()
	for _, chat_frame in ipairs(chat_frames) do
		if self:IsHooked(chat_frame) then
			self:Unhook(chat_frame)
		end
	end
end

function addon:switch_chat_frames(unhook)
	if not unhook then
		self:hook_chat_frames()
	else
		self:unhook_chat_frames()
	end
end
