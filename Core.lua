﻿local addon_name = ...
local addon = LibStub("AceAddon-3.0"):GetAddon(addon_name)

function addon:OnInitialize(name)
	self.db = LibStub("AceDB-3.0"):New("TR_DB", self.defaults, true)
	addon:load_locales()
	self.L = LibStub("AceLocale-3.0"):GetLocale(addon_name)
	ITEM_BONUS = '(.+) ('..self:get_of()..' .+)'
	self.options = self:generate_options()
	self:AddTranslateMenu(self.Dependences, self.options.args.translation, 'Translator.db.profile.translation')
	self:AddEasyMenu(self.DatabaseMenu, self.options.args.databases, 'Translator.db.profile.db')
	-- addon:AddApiMenu(addon.API, options.args.api_translation)
	-- addon:AddTranslateMenu(addon.TooltipMenu, options.args.translation, 'Translator.db.profile.translation')


	ChatFrame1:AddMessage(addon_name..' v'..GetAddOnMetadata(addon_name, 'Version')..self.L[" loaded. Type /tr or /translator for help."], 0.6, 0.8, 0.9)

	self.tmp = {} -- tmp storage for different caching

	self:CreateMinimapButton()
	self:SetupOptions()
	self:RegisterChatCommand("tr", "ChatCommand")
	self:RegisterChatCommand("translator", "ChatCommand")

	self:RegisterEvent('ADDON_LOADED')
	self.ScheduleTimers = {}
end

function addon:get_of()
	local game_locale = GetLocale()
	if game_locale == 'enUS' then
		return 'of'
	elseif game_locale == 'ruRU' then
		return 'с'
	end
	return ''
end

function addon:load_locales()
    local db_language = self.db.profile.language
    if TRANSLATOR_LOCALE_LOADER[db_language] then
    	TRANSLATOR_LOCALE_LOADER[db_language]()
    else
    	TRANSLATOR_LOCALE_LOADER['enUS']()
    end
end

function addon:OnEnable()
	self:LoadDb()
	-- self:LoadEmotePatterns()

	self:LoadTooltips()
	if self.db.profile then
		self.ScheduleTimers.BubbleFrame_Update = self:ScheduleRepeatingTimer('BubbleFrame_Update' , self.db.profile.bubbles)
 	end

	local db = self.db.profile
	for _, group in pairs(self.API) do
		for _, o in pairs(group) do
			if _G[o] and db[o] and not self:IsHooked(o) then
				self:Hook(o, true)
			end
		end
	end
	self:ToogleAPI()
	self:LoadGlobalStrings()
end

function addon:ChatCommand(input)
	if not input or input:trim() == "" then
		--InterfaceOptionsFrame_OpenToCategory(self.optionsFrame)
		LibStub("AceConfigDialog-3.0"):Open(addon_name) -- ОТДЕЛЬНО
	else
		LibStub("AceConfigCmd-3.0"):HandleCommand("servtr", addon_name, input)
	end
end

function addon:ShotterSecureHook(method)
	local handler
	if self.AutoBlizzardHookData[method] then
		handler = function(...)
			self:AutoBlizzardHook(method, ...)
		end
	end
	self:SecureHook(method, handler)
end

function addon:ADDON_LOADED(eventName, addon_name)
	if addon_name == 'Blizzard_AuctionUI' then
		self:ShotterSecureHook('AuctionFrameBrowse_Update')
		self:ShotterSecureHook('AuctionFrameBid_Update')
		self:ShotterSecureHook('AuctionFrameAuctions_Update')
		self:ShotterSecureHook('AuctionsFrameAuctions_ValidateAuction')
	elseif addon_name == 'Blizzard_CraftUI' then
		self:ShotterSecureHook('CraftFrame_SetSelection')
	elseif addon_name == 'Blizzard_TradeSkillUI' then
		self:ShotterSecureHook('TradeSkillFrame_SetSelection')
	elseif addon_name == 'Blizzard_TrainerUI' then
		self:ShotterSecureHook('ClassTrainerFrame_Update')
	end
end

---------Utilite functions---------

function addon:CopyTable(into, from)
	for key, val in pairs(from) do
		if type(val) == 'table' then
		if not into[key] then into[key] = {} end
			self:CopyTable(into[key], val)
		else
			into[key] = val
		end
	end

	if #from then
		if #into ~= #from then
			-- print('Translator WARNING: not equals length')
		end
	end

	return into
end

-- I(iterate) if table have string keys then return false else it's iterate table - true
function addon:IsTableI(tab)
   for k, _ in pairs(tab) do
      if type(k) == 'string' then
         return false
      end
   end
   return true
end

function addon:GetTableI(tab)
   if type(tab) == 'string' then
   	tab = {tab}
   end
   local t = {}
   local flag = false
   for k, v in pairs(tab) do
      if type(k) == 'number' then
      	t[k] = v
      	flag = true
      end
   end
   if not flag then
   	t = nil
   end
   return t
end

function addon:SaveGlobalStrings(db_with_keys)
	local save_db = GetLocale()..'_global_strings'
	if not _G[save_db] and db_with_keys then
		_G[save_db] = {}
		for k, _ in pairs(db_with_keys) do
			_G[save_db][k] = _G[k]
		end
	end
end

function addon:LoadGlobalStrings()
	local db = _G[self.db.profile.language..'_global_strings']
	if not db then return end
	-- self:SaveGlobalStrings(db)
	for k, v in pairs(db) do
		_G[k] = v
	end
end

function addon:is_global_string_format(string)
	local first_part_v1 = string.match(string, "^[A-Z0-9]+_.*")
	local first_part_v2 = string.match(string, "^[A-Z0-9]+$")
	return first_part_v1 or first_part_v2
end

function addon:is_global_string_value(value)
	return type(value) == 'string' and not string.match(value, "%d+") and string.match(value, "[А-Яа-я]")
end

function addon:ExtractGlobalStrings()
	self.db.profile.extact_db = {}
	for key, value in pairs(_G) do
		if self:is_global_string_format(key) and self:is_global_string_value(value) then
			self.db.profile.extact_db[key] = value
		end
	end
end

function addon:LoadEmotePatterns()
	for _, v in ipairs(EMOTE_LIST) do
		if string.find(v, '%%s') then
			self.EMOTE_TEXT[v] = {{'creature_Name', all = true}}
		end
	end
end

-- for dynamic names/ %s in keys

addon.DatabaseMenu = {
	'creature_ai_texts',
	'creature_Name',
	'creature_SubName',
	'dbscript_string',
	'gameobject',
	'gossip_menu_option',
	'gossip_texts',
	'item_description',
	'item_name',
	'mangos_string',
	'npc_text',
	'page_text',
	'points_of_interest',
	'questgiver_greeting',
	'quest_details',
	'quest_EndText',
	'quest_objectives',
	'quest_ObjectiveText',
	'quest_OfferRewardText',
	'quest_RequestItemsText',
	'quest_title',
	'script_texts',
	'spell_auradescription',
	'spell_description',
	'spell_name',
	'itemrandomproperties',
	'itemset',
	'spellitemenchantment',
}

addon.SpecDbInfo = {
	creature_ai_texts     = {'creature_Name', allowed_nil_translate = true},
	dbscript_string       = {'creature_Name', allowed_nil_translate = true},
	mangos_string         = {'creature_Name', 'mangos_string', allowed_nil_translate = true},
	script_texts          = {'creature_Name', allowed_nil_translate = true},
	npc_text              = {allowed_nil_translate = true},
	quest_details         = {allowed_nil_translate = true},
	spell_description     = {allowed_nil_translate = true},
	spell_auradescription = {allowed_nil_translate = true},
}

function addon:LoadDb()
	for _, db_name in ipairs(self.DatabaseMenu) do
		db = _G[self.db.profile.language..'_'..db_name]
		_G[self.db.profile.language..'_'..db_name..'_spec'] = {
			dn = {}, --dynamic names
			dt = {} --dynamic target
		}
		spec = _G[self.db.profile.language..'_'..db_name..'_spec']
		local add_to_spec
		local temp_db = {}

		if not db then
			return
			-- Printd(self.db.profile.language..'_'..db_name)
		end
		for k, v in pairs(db) do
			add_to_spec = false
			--there are too problems:
			--I  %s - it's dynamic names
			if string.find(k, '%%s') then
				spec.dn[k] = {}
				self:CopyTable(spec.dn[k], self.SpecDbInfo[db_name])
				spec.dn[k].generate_pattern = v
				add_to_spec = true
			--II $dt - it's dynamic target (changes, depending on npc's sex)
			elseif string.find(k, '$dt:.*:.*;') then
				spec.dt[k] = v
				add_to_spec = true
			end
			if not add_to_spec then
				temp_db[k] = v
			end
		end
		local old_db = _G[self.db.profile.language..'_'..db_name]
		_G[self.db.profile.language..'_'..db_name] = temp_db
		temp_db = old_db
		temp_db = {}
	end
end

---------Iterator for objects with text like QuestWatchLine1 ... QuestWatchLine50
function addon.TextObjPairsIter(s, iter)
	iter = iter+1
	local obj = _G[s.prefix..iter..s.postfix]
	if not obj then return nil end
	if s.field then
		obj = obj[s.field]
	end
	if not obj then return nil end
	local text = obj:GetText()
	if not text then
		if not s.finish or iter >= s.finish then
			return nil
		else
			return addon.TextObjPairsIter(s, iter)
		end
	end
	return iter, text, obj
end

function addon:TextObjPairs(prefix, start, postfix, finish, field)
	if start then start = start - 1 end
	return self.TextObjPairsIter, {prefix = prefix or '', postfix = postfix or '', finish = finish, field=field}, start or 0
end

--use as debug - full view of classes/objects, print table with all fields and metatable
function addon:PrintTab(obj, max_depth, level, meta_level)
	if not obj then
		print('Root = nil')
		return
	end
	if not level then level = 0 end
	if not max_depth then max_depth = 299 end
	if level == 0 then
		obj = {Root = obj}
	end

	if not self.Table_stack then
		self.Table_stack = {}
	end
	local inset = strrep('   ', level)
	inset = '\19'..inset
	local close_flag = false
	local meta_flag = false
	for m_key, m_value in pairs(obj) do
		key = m_key
		value = m_value
		if type(key) == 'table' then
			key = 'table_key'
		elseif type(key) == 'function' then
			key = 'function'
		elseif type(key) == 'userdata' then
			key = 'userdata'
		end
		if type(value) == 'table' then
			if not self.Table_stack[value] and (level < max_depth) then
				print(inset..'['..key..'] = {')
				self.Table_stack[value] = true
				self:PrintTab(value, max_depth, level+1, obj, meta_level)
			else
				close_flag = true
				print(inset..'['..key..'] = {...}')
			end
		elseif type(value) == 'function' then
			print(inset..'['..key..'] = function')
		elseif type(value) == 'userdata' then
			local user_data = 'userdata'
			if pcall(function() obj:GetName() end) then
				user_data = obj:GetName() or ''
			end
			print(inset..'['..key..'] = '..user_data)
		elseif type(value) == 'boolean' then
			print(inset..'['..key..'] = '..tostring(value))
		else
			print(inset..'['..key..'] = '..value)
		end
	end
	local meta = getmetatable(obj)
	if meta and meta_level   then
		if not self.Table_stack[meta] and (level < max_depth) then
			print(inset..'[metatable] = {')
			self.Table_stack[meta] = true
			self:PrintTab(meta, max_depth, level+1, obj, meta_level)
		else
			meta_flag = true
			print(inset..'['..key..'] = {...}')
		end
	end
	if not close_flag and level > 0 then
		inset = strrep('   ', level-1)
		inset = '\19'..inset
		print(inset..'}')
	end
	if level == 0 then
		for k, v in pairs(self.Table_stack) do
			self.Table_stack[k] = nil
		end
		self.Table_stack = nil
	end
end

function addon:str_in_byte(str)
	str = gsub(str, ".[\128-\191]*", function(w)
	         	local temp = {}
	         	for i = 1, strlen(w) do
	         		temp[i] = strbyte(w, i)
	         	end
	         	w = w..":"..table.concat(temp, ' ')..'\n'
	         	print(w)
	         	return w
	      end)
	return str
end

function addon:In(val, tab)
	for _, v in pairs(tab) do
		if v == val then
			return true
		end
	end
	return nil
end

function addon:ItemsWithBonus(text, method, object)
	if not text then return nil end
	local name = self.Translator:Translate(text, {ITEM_BONUS = 'item_name'}, method, object)
	return name
end

function addon:IsEventInChatGroup(event, group)
	if not group then return nil end

	for _, s in pairs(self.ChatFrameGroups[group]) do
		if s == event then return true end
	end
	return nil
end

function addon:IsEventBubble(event)
	local bubbles = {'CHAT_MSG_MONSTER_SAY', 'CHAT_MSG_MONSTER_YELL', 'CHAT_MSG_MONSTER_PARTY'}
	for _, v in pairs(bubbles) do
		if v == event then return true end
	end
	return nil
end

function addon:BubbleFrame_Update()
	local chatbubbles = C_ChatBubbles:GetAllChatBubbles()
	for _, chatbubble in ipairs(chatbubbles) do
		local textures = {chatbubble:GetRegions()}
		for _, object in pairs(textures) do
			if object:GetObjectType() == 'FontString' then
				local text = object:GetText()
				if object.last_text and object.last_text == text then
					return
				end
				local trans = self.Translator:Translate(text, {'script_texts', 'dbscript_string', 'creature_ai_texts'})
				if trans then object:SetText(trans) end
				object.last_text = object:GetText()
			end
		end
	end
end
