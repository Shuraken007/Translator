local addon_name = ...
local addon = LibStub("AceAddon-3.0"):GetAddon(addon_name)
local LDB = LibStub('LibDataBroker-1.1')
local LDBIcon = LibStub('LibDBIcon-1.0')
local LibQTip = LibStub('LibQTip-1.0')

function addon:CreateMinimapButton()
	local icon_obj = LDB:NewDataObject(addon_name, {
		icon = 134330, -- 'Interface\\Icons\\inv_misc_note_04'
		OnClick = function(self, button)
			if button ~= 'RightButton' then return end
			InterfaceOptionsFrame_OpenToCategory(addon_name)
			InterfaceOptionsFrame_OpenToCategory(addon_name)
			--TODO needed recall for correct open settings
		end,
		OnEnter = function(self)
			if LibQTip:IsAcquired(addon_name..'MiniMapTooltip') then return end

			self.tooltip = LibQTip:Acquire(addon_name..'MiniMapTooltip', 2, 'LEFT', 'RIGHT')
			local tooltip = self.tooltip

			local header_font = CreateFont(addon_name..'HeaderFont')
			header_font:SetFont('GameTooltipHeaderText', 16)
			header_font:SetTextColor(.41, .80, .94)
			tooltip:SetHeaderFont(header_font)

			tooltip:SetFont(GameFontNormal)

			local header = tooltip:AddHeader()
			tooltip:SetCell(header, 1, addon_name..' '..GetAddOnMetadata(addon_name, 'Version'), header_font, 'CENTER', 2)
			tooltip:AddSeparator(6, nil, nil, nil, 0)

			for _, db in pairs(addon.DatabaseMenu) do
				local value = addon.db.profile.db[db]
				tooltip[db] = tooltip:AddLine(db..':', value and addon.L["On"] or addon.L["Off"])
				tooltip:SetCellTextColor(tooltip[db], 2, value and 0 or 1, value and 1 or 0, 0, 1)
				tooltip:SetLineScript(tooltip[db], 'OnMouseDown', addon.ToggleMinimapButtonOptions, db)
			end
			tooltip:AddSeparator(10, nil, nil, nil, 0)

			local hint_line = tooltip:AddLine()
			tooltip:SetCell(hint_line, 1, '|cff00ff00'..addon.L["Right click on the minimap button to open settings.\nClick on the point tooltip to quickly manage the addon."]..'|r', nil, 'LEFT', 2)

			tooltip:SmartAnchorTo(self)
			tooltip:Show()
		end,
		OnLeave = function(self)
			self.tooltip:SetAutoHideDelay(0.15, self)
		end
	})
	LDBIcon:Register(addon_name, icon_obj, addon.db.profile.minimapIcon)
	self.minimapButton = LDBIcon:GetMinimapButton(addon_name)
end

function addon:ToggleMinimapButtonOptions(arg)
	local value = addon.db.profile.db[arg]
	addon.db.profile.db[arg] = not value

	local line = addon.minimapButton.tooltip[arg]
	addon.minimapButton.tooltip:SetCell(line, 2, addon.db.profile.db[arg] and addon.L["On"] or addon.L["Off"])
	addon.minimapButton.tooltip:SetCellTextColor(line, 2, addon.db.profile.db[arg] and 0 or 1, addon.db.profile.db[arg] and 1 or 0, 0, 1)

	LibStub("AceConfigRegistry-3.0"):NotifyChange(addon_name)
end
