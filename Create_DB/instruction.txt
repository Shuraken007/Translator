1. You need Mysql Server
2. Install python 2.7
make sure, that pip was installed too or install it
(
    google how to install python, pip and add them to PATH
    as example
    https://github.com/BurntSushi/nfldb/wiki/Python-&-pip-Windows-installation
)
3. Install previous vers of mysql-connector
open terminal (cmd):
    pip install mysql-connector==2.1.6 for python 2.7
4. Install libs which CreateDb need with same way: configparser, yandex_translate
5. Duplicate ConfigExample.txt with name Config.txt and fill required fields:

[connector_python]
user = root                            // your login at mysql
password = 1234                        // your password at mysql

[options]
generate_db = translator               // tmp name of new database (script will create this database, make all convertings and load tables from this)
mangos_db = mangos                     // main database, script will load all tables from this
addon_path = C:\Program Files (x86)\World of Warcraft\_classic_\Interface\AddOns\Translator

6. If you want use funcs copy_tables and fill_locale_db - you should load mangos database from CMangos (check github)
(fun part)
If you want to use autotranslate to any language - add yandex_keys (you may grep gitlab/github or create anoth yourself keys, or rewrite this part with any other api)
