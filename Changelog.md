## Next Release
* Breaking Changes
* Features
* Improvements
* Bug Fixes
  
## 2.1 (2019-10-12)
* Fixed StaticPopup_Show
  
## 2.0 (2019-09-16)
* Initial Release