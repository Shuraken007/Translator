local addon_name = ...
local addon = LibStub("AceAddon-3.0"):NewAddon(addon_name, "AceConsole-3.0", "AceEvent-3.0", "AceHook-3.0", "AceTimer-3.0")
Translator = addon

--preload functions
ST_N = UnitName('player')
ST_R = UnitRace('player')
ST_C = UnitClass('player')

st_n = strlower(UnitName('player'))
st_r = strlower(UnitRace('player'))
st_c = strlower(UnitClass('player'))

ST_S = UnitSex('player')
ST_G = function(text1, text2)
   return gsub(gsub(ST_S , "2", text1), "3", text2)
end

addon.defaults = {
	profile = {
		minimapIcon = {
			hide = false,
			minimapPos = 220,
			radius = 80,
		},
		translation = {
			['*'] = {
				['*'] = true
			},
			objects = {
				['*'] = {
					['*'] = {
						['*'] = true
					}
				},
			}
		},
		db = {
			['*'] = true
		},
		-- ruRU = true,
		language = 'enUS',
		nameplates = 1,
		bubbles = .01,
		api = {
			['*'] = false
		},
		ImaginaryModeOff = false,
	}
}
