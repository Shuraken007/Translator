﻿local addon_name = ...
local addon = LibStub("AceAddon-3.0"):GetAddon(addon_name)

addon.tooltip_cache = {}

local MAX_LINES = 999

local function get_configs()
	return {
		Other = {
			quest_title = {'quest_title', s = 1},
			npc_name = {'creature_Name', s = 1},
			object_name = {'gameobject', s = 1},
			points = {'points_of_interest', s = 1},
			npc_subname = {'creature_SubName', s = 2},
		},
		Item = {
			item_name = {'item_name', s = 1},
			-- recipe_name = {
			-- 	pattern_list = {
			-- 		["\n%s"] = 'item_name'
			-- 	},
			-- 	s = 4
			-- },
			bonus_items = {
				pattern_list = {ITEM_BONUS = 'item_name'},
				s = 1,
			},
			item_desc = {'item_description', s = 2, f = MAX_LINES},
			item_sets = {
				pattern_list = {
					ITEM_SET = 'item_name'
				},
				s = 5,
				f = MAX_LINES
			},
			descriptions_non_colored_at_header = {
				pattern_list = {
					USE_SPELL = 'spell_description',
				},
				s = 3,
			},
			-- descriptions_restore_colors_on_fail = {
			-- 	pattern_list = {
			-- 		USE_SPELL = {generate_pattern = '|cff20ff20%s|r'},
			-- 		EQUIP_ENCHANSMENT = {generate_pattern = '|cff20ff20%s|r'},
			-- 	},
			-- 	s = 2,
			-- 	f = MAX_LINES
			-- },
			descriptions = {
				pattern_list = {
					USE_SPELL = {'spell_description'},
					EQUIP_ENCHANSMENT = {'spell_description'},
					-- USE_SPELL = {'spell_description', generate_pattern = '|cff20ff20' .. USE_SPELL ..'|r'},
					-- EQUIP_ENCHANSMENT = {'spell_description', generate_pattern = '|cff20ff20' .. EQUIP_ENCHANSMENT .. '|r'},
				},
				s = 2,
				f = MAX_LINES
			}
		},
		Spell = {
			spell_name = {'spell_name', s = 1},
			aura_description = {'spell_auradescription', s = 2, f=MAX_LINES},
			spell_description = {'spell_description', s = 2, f=MAX_LINES},
		}
	}
end

function addon:LoadTooltips()
	local configs = get_configs()
	-- local item_functions = {'SetHyperlink', 'SetAuctionItem', 'SetAuctionSellItem', 'SetBagItem', 'SetBuybackItem', 'SetInboxItem', 'SetInventoryItem', 'SetLootItem', 'SetLootRollItem', 'SetMerchantItem', 'SetQuestItem', 'SetQuestLogItem', 'SetSendMailItem', 'SetTradePlayerItem', 'SetTradeSkillItem', 'SetTradeTargetItem'}
	-- local spell_functions = {'SetAction','SetPetAction','SetQuestLogRewardSpell','SetQuestRewardSpell','SetShapeshift','SetTalent','SetUnit','SetUnitAura','SetUnitBuff','SetUnitDebuff'}
	self:HookScript(GameTooltip, "OnTooltipSetItem", function(...)
		self:TooltipOnUpdate({
			tooltip = 'GameTooltip',
			config = configs.Item,
			cache_key = 'GameTooltip' .. "OnTooltipSetItem"
		})
	end)

	self:HookScript(GameTooltip, "OnTooltipSetSpell", function(...)
		self:TooltipOnUpdate({
			tooltip = 'GameTooltip',
			config = configs.Spell,
			cache_key = 'GameTooltip' .. "OnTooltipSetSpell"
		})
	end)

	self:HookScript(GameTooltip, 'OnUpdate', function(...)
		self:TooltipOnUpdate({
			tooltip = 'GameTooltip',
			config = configs.Other,
			cache_key = 'GameTooltipOnOther'
		})
	end)
	self:SecureHook(ShoppingTooltip1, 'SetCompareItem', function(...)
		self:TooltipOnUpdate({
			tooltip = 'ShoppingTooltip1',
			config = configs.Item,
			cache_key = 'ShoppingTooltip1SetCompareItem'
		})
	end)
	self:SecureHook(ShoppingTooltip2, 'SetCompareItem', function(...)
		self:TooltipOnUpdate({
			tooltip = 'ShoppingTooltip2',
			config = configs.Item,
			cache_key = 'ShoppingTooltip2SetCompareItem'
		})
	end)
end

function addon:TooltipOnUpdate(params)
	local tooltip   = params.tooltip
	local config    = params.config
	local cache_key = params.cache_key
	if not self.tooltip_cache[cache_key] then
		self.tooltip_cache[cache_key] = {
			last_translate = {},
			prev_first_line = '',
		}
	end
	local cache = self.tooltip_cache[cache_key]
	local prev_first_line = cache.prev_first_line
	local last_translate = cache.last_translate


	local left_str = _G[tooltip .. 'TextLeft1']:GetText() or ''
	local right_str = (_G[tooltip .. 'TextRight1']:IsVisible() and _G[tooltip .. 'TextRight1']:GetText()) or ''
	local firstline = left_str..right_str

	local cond1 = (firstline == prev_first_line)
	local cond2 = last_translate[1] and (left_str == last_translate[1][1])
	if not (cond1 or cond2) then
		cache.prev_first_line = firstline
		self:TranslateTooltip({
			tooltip = tooltip,
			config  = config,
			cache   = cache
		})
	else
		self:RestoreTooltip(tooltip, last_translate)
	end
end

function addon:RestoreTooltip(tooltip, last_translate)
	for k, v in pairs(last_translate) do
		_G[tooltip .. 'TextLeft'..k]:SetText(v[1])
		_G[tooltip .. 'TextLeft'..k]:SetTextColor(unpack(v[2]))
	end
end

function addon:TranslateTooltip(params)
	local tooltip = params.tooltip
	local config  = params.config
	local cache   = params.cache

	local replace
	local reset_flag
	cache.last_translate = {}
	for i, text, str in self:TextObjPairs(tooltip .. 'TextLeft') do
		for key, pattern in pairs(config) do
			if pattern.s == i or (pattern.f and pattern.s < i and pattern.f >= i) then
				if not pattern.pattern_list then
					local plist = {SplitText = { {pattern[1], COLOR_STR = {nil, pattern[1]}, all = true}} }
					replace = self.Translator:Translate(text, plist, tooltip)
				else
					replace = self.Translator:Translate(text, pattern.pattern_list, tooltip)
				end
				if replace then
					local text_color = {str:GetTextColor()}
					cache.last_translate[i] = {replace, text_color}
					str:SetText(replace)
					str:SetTextColor(unpack(text_color))
					reset_flag = true
					break
				end
			end
		end
	end
	if reset_flag then
		if _G[tooltip]:IsShown() and _G[tooltip]:IsVisible() then
			_G[tooltip]:Show()
		end
	end
end

--should check all \n and split text to array
function SplitText(mode, text)
	if mode then
		local text_arr = {}
		for _t in string.gmatch(text, '(.-)\10') do
			text_arr[#text_arr + 1] = _t
		end
		if #text_arr > 0 then
			text_arr[#text_arr + 1] = string.match(text, '.+\10(.-)$')
		else
			text_arr = {text}
		end
		return text_arr
	else
		local result = ''
		for i = 1, #text do
			if i > 1 then result = result..'\n' end
			result = result..text[i]
		end
		return result
	end
end
