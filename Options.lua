﻿local addon_name = ...
local addon = LibStub("AceAddon-3.0"):GetAddon(addon_name)

----------------tables to generate menu-----------------

addon.ReadyDB = {
	['enUS'] = 'English',              --0
	--['koKR'] = '한국어',                    -- 1
	--['frFR'] = 'Française',            -- 2
	--['deDE'] = 'Deutsch',              -- 3
	--['zhCN'] = '中文 (simplified)',       -- 4
	--['zhTW'] = '中文 (traditional)',      -- 5
	--['esES'] = 'Español (castellano)', -- 6
	--['esMX'] = 'Español (mexicano)',   -- 7
	['ruRU'] = 'Русский',              -- 8
}

addon.API = {
	['Container/Bag'] = {'GetBagName'},
	Loot = {
		'GetLootRollItemInfo',
		'GetLootSlotInfo'
	},
	Gossip = {'GetGossipText', 'GetGossipOptions'},
	Mail = {'GetInboxInvoiceInfo'},
	Merchant = {'GetMerchantItemInfo'},
	Trade = {
		'GetTradePlayerItemInfo',
		'GetTradeTargetItemInfo'
	},
	Training = {'GetTrainerGreetingText'},
	Quest = {
		'GetGreetingText',
		'GetTitleText',
		'GetObjectiveText',
		'GetQuestText',
		'GetProgressText',
		'GetRewardText',
		'GetQuestItemInfo',
		'GetQuestLogTitle',
		'GetQuestLogLeaderBoard',
		'GetQuestLogQuestText',
		'GetQuestLogChoiceInfo',
		'GetQuestLogRewardInfo'
	},
	Unit = {'UnitName'}
}

addon.Dependences = {
	Quests = {
		ChatFrame = {'quest_title'},
		GossipFrameUpdate = {'quest_title'},
		QuestLogFrame = {
			QuestLog_Update = {'quest_title'},
			QuestLog_UpdateQuestDetails = {'quest_title', 'quest_objectives', 'quest_details', 'quest_ObjectiveText', 'quest_EndText'}
		},
		QuestFrame = {
			QuestFrameDetailPanel_OnShow = {'quest_title', 'quest_objectives', 'quest_details'},
			QuestFrameProgressPanel_OnShow = {'quest_title', 'quest_RequestItemsText'},
			QuestFrameRewardPanel_OnShow = {'quest_title', 'quest_OfferRewardText'},
			QuestFrameGreetingPanel_OnShow = {'quest_title'}
		},
		QuestWatchFrame = {
			QuestWatch_Update = {'quest_title', 'quest_ObjectiveText', 'quest_EndText'},
		},
		StaticPopup_Show = {'quest_title', 'item_name'},
		UIErrorsFrame_OnEvent = {'quest_ObjectiveText', 'quest_EndText'}
	},
	Items = {
		AuctionFrameAuctions_Update = {'item_name'},
		AuctionFrameBid_Update = {'item_name'},
		AuctionFrameBrowse_Update = {'item_name'},
		AuctionsFrameAuctions_ValidateAuction = {'item_name'},
		-- AddMessage = {object = 'ChatFrameEditBox', 'item_name'},
		ChatFrame = {'item_name'},
		ContainerFrame_GenerateFrame = {'item_name'},
		CraftFrame_SetSelection = {'item_name'},
		GroupLootFrame_OnShow = {'item_name'},
		InboxFrame_Update = {'item_name'},
		ItemTextFrame = {'item_name'},
		LootFrame_Update = {'item_name'},
		MerchantFrame_UpdateBuybackInfo = {'item_name'},
		MerchantFrame_UpdateMerchantInfo = {'item_name'},
		OpenMail_Update = {'item_name'},
		QuestInfo_ShowRewards = {'item_name'},
		QuestFrameItems_Update = {'item_name'},
		QuestFrameProgressItems_Update = {'item_name'},
		QuestLog_UpdateQuestDetails = {'item_name'},
		QuestWatch_Update = {'item_name'},
		SendMailFrame_Update = {'item_name'},
		StaticPopup_Show = {'item_name'},
		TradeFrame_UpdatePlayerItem = {'item_name'},
		TradeFrame_UpdateTargetItem = {'item_name'},
		TradeSkillFrame_SetSelection = {'item_name'},
		UIErrorsFrame_OnEvent = {'item_name'}
	},
	NPC = {
		BankFrame_ShowPanel = {'creature_Name'},
		ChatFrame = {'creature_Name'},
		ClassTrainerFrame_Update = {'creature_Name', 'mangos_string'},
		CompactUnitFrame_UpdateName = {'creature_Name'},
		GossipFrameUpdate = {'npc_text', 'creature_Name', 'gameobject', 'gossip_menu_option', 'gossip_texts'},
		GuildRegistrar_OnShow = {'creature_Name'},
		ItemTextFrame = {'gameobject'},
		MerchantFrame_UpdateMerchantInfo = {'creature_Name'},
		QuestFrameGreetingPanel_OnShow = {'npc_text'},
		QuestFrame_SetPortrait = {'creature_Name'},
		QuestLog_UpdateQuestDetails = {'creature_Name'},
		QuestWatch_Update = {'creature_Name'},
		TabardFrame_OnEvent = {'creature_Name'},
		TaxiFrame_OnEvent = {'creature_Name'},
		UnitFrame_Update = {'creature_Name'},
		UIErrorsFrame_OnEvent = {'creature_Name'},
	},
	Books = {
		ItemTextFrame = {'page_text'}
	},
	Tooltips = {
		GameTooltip = {'creature_Name', 'gameobject', 'item_name', 'item_description'},
		ItemRefTooltip = {'item_name', 'item_description'},
		ShoppingTooltip1 = {'item_name'},
		ShoppingTooltip2 = {'item_name'}
	},
	['Phrases/Messages'] = {
		ChatFrame = {'creature_ai_texts', 'script_texts', 'dbscript_string', 'creature_Name'}
	},
	Other = {
		ChatFrame = {'mangos_string'}
	}
}

--fictious field means that function hooked other way,
--but it's easier to locate it here for correct db work,
--not so many such strange functions
addon.HookFunctions = {
	--[[       ]]GameTooltip = 'fictitious',
	--[[       ]]ItemRefTooltip = 'fictitious',
	--[[       ]]ShoppingTooltip1 = 'fictitious',
	--[[       ]]ShoppingTooltip2 = 'fictitious',
	--[[checked]]AuctionFrameAuctions_Update = {},
	--[[checked]]AuctionFrameBid_Update = {},
	--[[checked]]AuctionFrameBrowse_Update = {},
	--[[replace]]AuctionsFrameAuctions_ValidateAuction = {'GetAuctionSellItemInfo'}, --replace AuctionSellItemButton_OnEvent
	--[[replace]]BankFrame_ShowPanel = {'UnitName'}, --replace BankFrame_OnEvent
	-- [[       ]]ChatFrameEditBox = {},
	--[[       ]]ChatFrame = {}, --fixed, but need tests
	--[[checked]]CompactUnitFrame_UpdateName = {'UnitName'},
	--[[       ]]ClassTrainerFrame_Update = {'UnitName', 'GetTrainerGreetingText'},
	--[[checked]]ContainerFrame_GenerateFrame = {'GetBagName'},
	--[[       ]]CraftFrame_SetSelection = {},
	--[[checked]]GossipFrameUpdate = {'UnitName', 'GetGossipText', 'GetGossipOptions'},
	--[[       ]]GroupLootFrame_OnShow = {'GetLootRollItemInfo'},
	--[[       ]]GuildRegistrar_OnShow = {'UnitName'},
	--[[checked]]InboxFrame_Update = {'GetInboxHeaderInfo', 'GetItemInfo'},
	--[[       ]]ItemTextFrame = {'ItemTextGetText', funcs = {OnEvent = {}}},
	--[[checked]]LootFrame_Update = {'GetLootSlotInfo'},
	--[[checked]]MerchantFrame_UpdateBuybackInfo = {},
	--[[checked]]MerchantFrame_UpdateMerchantInfo = {'UnitName', 'GetMerchantItemInfo'},
	--[[checked]]OpenMail_Update = {'GetInboxInvoiceInfo'},
	--[[checked]]QuestFrameDetailPanel = {'GetTitleText', 'GetObjectiveText', 'GetQuestText', funcs = {OnShow = {}}},
	--[[replace]]QuestFrameGreetingPanel = {'GetGreetingText', funcs = {OnShow = {}}}, --replace QuestFrameGreetingPanel_OnShow
	--[[checked]]QuestFrameItems_Update = {'GetQuestItemInfo', 'GetQuestLogChoiceInfo', 'GetQuestLogRewardInfo'},
	--[[checked]]QuestFrameProgressItems_Update = {'GetQuestItemInfo'},
	--[[checked]]QuestFrameProgressPanel = {'GetTitleText', 'GetProgressText', funcs = {OnShow = {}}},
	--[[checked]]QuestFrameRewardPanel = {'GetTitleText', 'GetRewardText', funcs = {OnShow = {}}},
	--[[checked]]QuestFrame_SetPortrait = {'UnitName'},
	--[[checked]]QuestLog_Update = {'GetQuestLogTitle'},
	--[[checked]]QuestLog_UpdateQuestDetails = {'GetQuestLogTitle', 'GetQuestLogLeaderBoard', 'GetQuestLogQuestText'},
	--[[checked]]QuestWatch_Update = {'GetQuestLogTitle', 'GetQuestLogLeaderBoard'},
	--[[added  ]]QuestInfo_ShowRewards = {'GetQuestLogChoiceInfo'},
	--[[checked]]SendMailFrame_Update = {'GetSendMailItem'},
	--[[replace]]StaticPopup_Show = {'GetAbandonQuestName'}, --replace StaticPopup_OnUpdate
	--[[       ]]TabardFrame_OnEvent = {'UnitName'},
	--[[       ]]TaxiFrame_OnEvent = {'UnitName'},
	--[[       ]]TradeFrame_UpdatePlayerItem = {'GetTradePlayerItemInfo'},
	--[[       ]]TradeFrame_UpdateTargetItem = {'GetTradeTargetItemInfo'},
	--[[       ]]TradeSkillFrame_SetSelection = {},
	--[[checked]]UIErrorsFrame = {funcs = {OnEvent = {}}}, --possible bug with Clearing messages ?
	--[[       ]]UnitFrame_Update = {'UnitName'},
}

---------Search in Servtr.HookFunctions and return text with api dependence------
function addon:Compensated(method, object)
	local result = ''
	if (not self.HookFunctions[method] and
			(not object or (object and not ( self.HookFunctions[object].funcs and self.HookFunctions[object].funcs[method])))) or
		(self.HookFunctions[method] and type(self.HookFunctions[method]) == 'string' and
			self.HookFunctions[method] == 'fictitious') then
		-- if object then self:PrintTab(self.HookFunctions[object].funcs[method]) end
		return result
	else
		local api_list

		if not object then
			api_list = self.HookFunctions[method]
		else
			api_list = self.HookFunctions[object].funcs[method]
		end
		if #api_list == 0 then
			return result
		end
		result = '\n\n'..self.L["Is compensated with following API hooks:"]
		for _, api in ipairs(api_list) do
			result = result..'\n'..api
		end
	end
	return result
end

---------path should looks like 'self.db.profile.translation'
function addon:GetPath(path)
	if not path then return nil end
	local db
	local flag = false
	for key in string.gmatch(path, '(.-)%.') do
		if not db then
			db = _G[key]
			flag = true
		elseif db[key] then
			db = db[key]
			flag = true
		else
			db = nil
		end
	end
	if db then
		local key = string.match(path, '.+%.(.+)')
		if key and db[key] then
			db = db[key]
		elseif key then
			db = nil
		end
	elseif not flag then
		db = _G[path]
	end

	return db
end

---------Add info to options menu with short simple table---------
function addon:AddEasyMenu(from, to, path)
	if not to.args or type(to.args) ~= 'table' then
		to.args = {}
	end
	for k, v in pairs(from) do
		if type(v) == 'table' then
			to.args[k] = {
				type = 'group',
				name = self.L[k] or k,
				desc = self.L[k..'_desc'] or k,
			}
			self:AddEasyMenu(v, to.args[k], path..'.'..k)
		elseif type(v) == 'string' then
			local path = path
			local opt = v
			to.args[v] = {
				type = 'toggle',
				name = self.L[v] or v,
				desc = self.L[v..'_desc'] or v,
				get = function() local db = self:GetPath(path) return db[opt] end,
				set = function(self, state) local db = addon:GetPath(path) db[opt] = not db[opt] end,
			}
		end
	end
end

function addon:AddTranslateMenu(from, to, path)
	if not to.args or type(to.args) ~= 'table' then
		to.args = {}
	end
	for k, v in pairs(from) do
		if type(v) == 'table' and (self.HookFunctions[k]) then
			to.args[k] = {
				type = 'group',
				name = self.L[k] or k,
				desc = (self.L[k..'_desc'] or k)..self:Compensated(k),
			}
			self:AddTranslateMenu(v, to.args[k], path..'.'..k)
		elseif type(v) == 'table' and (v.object and self.HookFunctions[v.object]) then
			to.args[k] = {
				type = 'group',
				name = self.L[v.object..':'..k] or v.object..':'..k,
				desc = (self.L[v.object..':'..k..'_desc'] or v.object..':'..k)..self:Compensated(k, v.object),
			}
			self:AddTranslateMenu(v, to.args[k], path..'.objects.'..v.object..'.'..k)
		elseif type(v) == 'table' then
			to.args[k] = {
				type = 'group',
				name = self.L[k] or k,
			}
			self:AddTranslateMenu(v, to.args[k], path)
		elseif k ~= 'object' and type(v) == 'string' then
			local path = path
			local opt = v
			local desc = self.L[v..'_desc'] or v
			-- spetial for Global Strings
			if _G[v] and type(_G[v]) == 'string' then
				desc = _G[v]
			end
			to.args[v] = {
				type = 'toggle',
				name = self.L[v] or v,
				desc = desc,
				get = function() local db = self:GetPath(path) return db[opt] end,
				set = function(self, state) local db = addon:GetPath(path) db[opt] = not db[opt] end,
			}
		end
	end
end

function addon:AddApiMenu(from, to)
	local i = 0
	for k, v in pairs(from) do
		i = i + 1
		to.args[k] = {
			order = i,
			type = 'header',
			name = self.L[k..' Functions'] or k..' Functions',
		}
		for _, opt in pairs(v) do
			i = i + 1
			local option = opt
			to.args[opt] = {
				order = i,
				type = 'toggle',
				name = option,
				desc = self.L[option..'_desc'] or option,
				get = function() return self.db.profile.api[option] end,
				set = function(self, state)
					addon.db.profile.api[option] = state
					addon:ToogleAPI(option)
				end,
			}
		end
	end
end

function addon:SwitchApi(opt, db)
	if opt and _G[opt] then
		if db[opt] and not self:IsHooked(opt) then
			local handler
			if self.AutoApiHookData[opt] then
				handler = function(...)
					return self:AutoApiHook(opt, ...)
				end
			end
			self:Hook(opt, handler, true)
		elseif not db[opt] and self:IsHooked(opt) then
			self:Unhook(opt)
		end
	end
end

function addon:CheckFunctionDependence(api_list, db, opt)
	-- flag: dependence exist
	-- unhook: all api from api_list are turned on => unhook blizzard func
	local flag, unhook = false, true
	if #api_list == 0 then
		unhook = false
		flag = true
	end
	for _, o in ipairs(api_list) do
		if not opt or o == opt then
			flag = true
		end
		if not db[o] then
			unhook = false
		end
	end
	return flag, unhook
end

addon.IgnoreCallFuncs = {
	"ChatFrame",
	"ContainerFrame_GenerateFrame",
}

function addon:SwitchBlizzardFuncHook(unhook, method, object, ...)
--2 cases - simple function like 'LootFrame_Update' and functions of some object like 'AddMessage' from UIErrorsFrame
	if not method then print('no method') return end
	local insecure
	local handler
	local key = method
	if object then key = object.."."..method end
	if not object then --I case
		if self.AutoBlizzardHookData[method] then
			handler = function(...)
				self:AutoBlizzardHook(method, ...)
				-- local v = {key, addon:GetTableI(arg), this = this}
				-- self.CallStack:add(v)
			end
		end
		if not handler and not self:In(method, self.IgnoreCallFuncs) then
			handler = function(...)
				-- local arg = {...}
				self[method](self, ...)
				-- local v = {key, addon:GetTableI(arg), this = this}
				-- self.CallStack:add(v)
			end
		end
		if unhook and self:IsHooked(method) then
			self:Unhook(method)
		elseif not unhook and not self:IsHooked(method) and _G[method] then
			insecure = self.HookFunctions[method].insecure
			if insecure then
				self:RawHook(method, handler)
			else
				self:SecureHook(method, handler)
			end
		end
	else -- II case
		insecure = self.HookFunctions[object].funcs[method].insecure
		if not _G[object] then
			return
		else
			if self.AutoBlizzardHookData[object] and self.AutoBlizzardHookData[object][method] then
				local object_as_str = object
				handler = function(...)
					-- local arg = {...}
					self:AutoBlizzardHook({object_as_str, method}, ...)
					-- local v = {key, addon:GetTableI(arg), this = this}
					-- self.CallStack:add(v)
				end
			end
			object = _G[object]
		end
		if not handler and not self:In(method, self.IgnoreCallFuncs) then
			handler = function(...)
				-- local arg = {...}
				-- local is_object = (type(this) == 'userobject')
				self[method](self, ...)
				-- local v
				-- if is_object then
				-- 	v = {key, addon:GetTableI(arg), this = this}
				-- else
				-- 	table.insert(arg, this)
				-- 	v = {key, addon:GetTableI(arg)}
				-- end
				-- self.CallStack:add(v)
			end
		end
		if unhook and self:IsHooked(object, method) then
			self:Unhook(object, method)
		elseif not unhook and not self:IsHooked(object, method) then
			if insecure then
				self:Hook(object, method, handler)
			else
				if object then
					self:SecureHookScript(object, method, handler)
				else
					self:SecureHook(object, method, handler)
				end
			end
		end
	end
end

function addon:ToogleAPI(opt)
	local db = self.db.profile.api

	--if opt is defined, then should switch it (Hook or Unhook)
	self:SwitchApi(opt, db)
	--if opt not defind, then turn on working api
	if not opt then
		for k, v in pairs(db) do
			if v then
				self:SwitchApi(k, db)
			end
		end
	end
	--Also should check Blizzard hooked functions, which depends from api (unhook them if all dependences work or hook if not all api turn on)
	local flag, unhook
	for k, v in pairs(self.HookFunctions) do
		if not(type(v) == 'string' and v == 'fictitious') then
			if not v.funcs then --key is not some object, it's global func
				flag, unhook = self:CheckFunctionDependence(v, db, opt)
				if flag then
					-- Chat Frames is spetial case
					-- TODO: refactor
					if k == 'ChatFrame' then
						self:switch_chat_frames(unhook)
					else
						self:SwitchBlizzardFuncHook(unhook, k)
					end
				end
			else  --key is object and have list of funcs
				for o_k, o_v in pairs(v.funcs) do
					flag, unhook = self:CheckFunctionDependence(o_v, db, opt)
					if flag then
						self:SwitchBlizzardFuncHook(unhook, o_k, k)
					end
				end
			end
		end
	end
end

function addon:generate_options()
	return {
		type = 'group',
		name = addon_name,
		args = {
			general = {
				order = 1,
				type = 'group',
				name = self.L["General"],
				args = {
					language = {
						order = 1,
						--width	= "full",
						type = 'select',
						name = self.L["Language"],
						desc = self.L["Set language for translate."],
						style = 'dropdown',
						confirm = true,
						confirmText = self.L["Are you sure you want to change the translation?"],
						get = function() return addon.db.profile.language end,
						set = function(self, locale)
							--for _, _lang in pairs(addon.ReadyDB) do
							--	addon.db.profile[_lang] = false
							--end
							--addon.db.profile[locale] = true
							addon.db.profile.language = locale
							-- addon:LoadGlobalStrings()
							-- addon:LoadEmotePatterns()
							addon:LoadDb()
						end,
						values = function() return addon.ReadyDB end,
					},
					bubble_interval = {
						order = 2,
						type = 'range',
						name = self.L["Chat Bubbles update interval"],
						desc = self.L["Set refresh interval for chat bubbles. Zero value will stop updating and translating."],
						min = 0,
						max = .5,
						step = .01,
						get = function() return addon.db.profile.bubbles end,
						set = function(self, time)
							addon.db.profile.bubbles = time
							if addon.ScheduleTimers.BubbleFrame_Update then
								addon:CancelTimer(addon.ScheduleTimers.BubbleFrame_Update)
							end
							if time > 0 then
								addon.ScheduleTimers.BubbleFrame_Update = addon:ScheduleRepeatingTimer('BubbleFrame_Update', time)
							end
						end,
					},
					resetDB = {
						order = 3,
						type = 'execute',
						name = "ResetDB",
						func = function() addon.db:ResetProfile() end,
					},
				},
			},
			translation = {
				order = 2,
				type = 'group',
				childGroups	= "select",
				name = self.L["Translation"],
				desc = self.L["Select categories to translate."],
				args = {}
			},
			databases = {
				order = 3,
				type = 'group',
				name = self.L["Databases"],
				desc = self.L["Global database switching.\nATTENTION! If you disable the databases in this menu, they will be disabled throughout the addon and will not be used. Use this menu to quickly enable / disable databases."],
				args = {}
			},
			api_translation = {
				order = 4,
				type = 'group',
				name = self.L["API localization"],
				desc = self.L["Full api translation can translate many things in other addons, but addons, which works with databases, containing original language info won't work."],
				args = {}
			},
		},
	}
end
-- addon:AddApiMenu(addon.API, options.args.api_translation)
-- addon:AddTranslateMenu(addon.TooltipMenu, options.args.translation, 'Translator.db.profile.translation')

function addon:SetupOptions()
	if LibStub:GetLibrary("LibAboutPanel-2.0", true) then
		self.options.args.aboutTab = LibStub("LibAboutPanel-2.0"):AboutOptionsTable(addon_name)
		self.options.args.aboutTab.order = 5
	end

	LibStub("AceConfigRegistry-3.0"):RegisterOptionsTable(addon_name, addon.options)
	self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions(addon_name)
end
